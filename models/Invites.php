<?php
namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%invites}}".
 *
 * @property string $invite
 */
class Invites extends ActiveRecord {
    public static function tableName()
    {
        return '{{%invites}}';
    }
}
