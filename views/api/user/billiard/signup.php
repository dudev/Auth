<?php
use general\ext\api\auth\AuthUrlCreator;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
/**
 * Project: Auth - Seven lights
 * User: Evgeny Rusanov
 * E-mail: admin@dudev.ru
 * Site: dudev.ru
 */

/* @var $this \yii\web\View */
$this->registerCssFile('/css/api/billiard/style.css');


$form = ActiveForm::begin([
	'id' => 'loginForm',
	'fieldConfig' => [
		'template' => '<div class="field">{input}</div>',
	],
]); ?>

	<div id="logo">
		<img src="/i/logo.jpg" alt="Семь огней - Seven lights" title="Семь огней - Seven lights">
	</div>

<?= $form->errorSummary($model, ['header' => '']) ?>

<?= $form->field(
	$model,
	'login',
	[
		'inputOptions' => [
			'placeholder' => $model->getAttributeLabel('login'),
		]
	]
) ?>
<?= $form->field(
	$model,
	'password',
	[
		'inputOptions' => [
			'placeholder' => $model->getAttributeLabel('password'),
		]
	]
)->passwordInput() ?>

	<div class="submit">
		<a href="#" id="forgot">Забыли пароль?</a><?= Html::submitButton('Регистрация') ?>
		<div class="subAction">
			Уже зарегистрированы? <a href="<?= AuthUrlCreator::userAuthenticate($service, 'login', $retUrl) ?>">Войти</a>
		</div>
	</div>

<?php ActiveForm::end(); ?>