<?php
use general\ext\api\auth\AuthUrlCreator;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
/**
 * Project: Auth - Seven lights
 * User: Evgeny Rusanov
 * E-mail: admin@dudev.ru
 * Site: dudev.ru
 */

/* @var $this \yii\web\View */
$this->registerCssFile('/css/api/sport/style.css');

$origin = Yii::$app->params['api']['origins'][ $service ];
$this->registerJs(<<<JS
$('#do_reg').click(function() {
    parent.location = '$origin' + $(this).attr('href');
});
JS
);

$form = ActiveForm::begin([
	'id' => 'loginForm',
	'fieldConfig' => [
		'template' => '<div class="field">{input}</div>',
	],
]); ?>

<?= $form->errorSummary($model, ['header' => '']) ?>

<?= $form->field(
	$model,
	'login',
	[
		'inputOptions' => [
			'placeholder' => $model->getAttributeLabel('login'),
		]
	]
) ?>
<?= $form->field(
	$model,
	'password',
	[
		'inputOptions' => [
			'placeholder' => $model->getAttributeLabel('password'),
		]
	]
)->passwordInput() ?>

	<div class="submit">
		<a href="#" id="forgot">Забыли пароль?</a><?= Html::submitButton('Вход') ?>
		<div class="subAction">
			Ещё не зарегистрированы? <a href="/site/signup" id="do_reg">Зарегистрироваться</a>
		</div>
	</div>

<?php ActiveForm::end(); ?>