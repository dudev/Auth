<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $model app\models\LoginForm */

$this->title = 'Аутентификация';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $form = ActiveForm::begin([
    'id' => 'loginForm',
    'fieldConfig' => [
        'template' => '<div class="field">{input}</div>',
    ],
]); ?>

<div id="logo">
    <img src="/i/logo.jpg" alt="Семь огней - Seven lights" title="Семь огней - Seven lights">
</div>

<?= $form->errorSummary($model, ['header' => '']) ?>

<?= $form->field(
    $model,
    'login',
    [
        'inputOptions' => [
            'placeholder' => $model->getAttributeLabel('login'),
        ]
    ]
) ?>
<?= $form->field(
    $model,
    'password',
    [
        'inputOptions' => [
            'placeholder' => $model->getAttributeLabel('password'),
        ]
    ]
)->passwordInput() ?>

<div class="submit">
    <a href="#" id="forgot">Забыли пароль?</a><?= Html::submitButton('Вход') ?>
</div>

<?php ActiveForm::end(); ?>
