<?php
/* @var $this yii\web\View */
$this->title = 'Главная';
?>
<div id="loginForm">
	<img src="/i/logo.jpg" alt="Семь огней - Seven lights" title="Семь огней - Seven lights">
    <div>Аутентификация прошла успешна</div>
    <p>
        <a href="<?= Yii::$app->urlManager->createUrl('site/logout') ?>">Выйти (<?= Yii::$app->user->identity->login ?>)</a>
    </p>
</div>
