<?php

use yii\db\Schema;
use yii\db\Migration;

class m141028_182732_create_secret_question_table extends Migration
{
    public function up()
    {
	    $this->createTable('secret_question', [
		    'id' => Schema::TYPE_BIGPK,
		    'user_id' => Schema::TYPE_BIGINT . ' NOT NULL',
		    'question' => Schema::TYPE_STRING . ' NOT NULL',
		    'answer' => Schema::TYPE_STRING . ' NOT NULL',
		    'created_at' => Schema::TYPE_INTEGER . ' NOT NULL',
		    'updated_at' => Schema::TYPE_INTEGER . ' NOT NULL',
	    ]);
	    $this->createIndex('user_id_idx', 'secret_question', 'user_id');
    }

    public function down()
    {
        echo "m141028_182732_create_secret_question_table cannot be reverted.\n";

        return false;
    }
}
