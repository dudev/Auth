<?php

use yii\db\Schema;
use yii\db\Migration;

class m141020_131342_create_user_table extends Migration
{
    public function up()
    {
        $this->createTable('user', [
            'id' => Schema::TYPE_BIGPK,
            'login' => Schema::TYPE_STRING . '(50) NOT NULL',
            'password_hash' => Schema::TYPE_STRING . '(60) NOT NULL',
            'status' => Schema::TYPE_BOOLEAN . ' NOT NULL',
            'created_at' => Schema::TYPE_INTEGER . ' NOT NULL',
            'updated_at' => Schema::TYPE_INTEGER . ' NOT NULL',
            'auth_key' => 'CHAR(64) NOT NULL'
        ]);
        $this->createIndex('login_idx', 'user', 'login', true);
    }

    public function down()
    {
        echo "m141020_131342_create_user_table cannot be reverted.\n";

        return false;
    }
}
