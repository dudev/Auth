<?php

use yii\db\Schema;
use yii\db\Migration;

class m150310_060835_add_ip_and_browser_fields_in_temporary_keys_table extends Migration
{
    public function up()
    {
	    $this->addColumn('temporary_keys', 'ip', Schema::TYPE_STRING . '(39) NOT NULL');
	    $this->addColumn('temporary_keys', 'browser', Schema::TYPE_STRING . ' NOT NULL');
    }

    public function down()
    {
        echo "m150310_060835_add_ip_and_browser_fields_in_temporary_keys_table cannot be reverted.\n";

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
