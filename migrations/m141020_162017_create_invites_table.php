<?php

use yii\db\Schema;
use yii\db\Migration;

class m141020_162017_create_invites_table extends Migration
{
    public function up()
    {
	    $this->createTable('invites', [
		    'invite' => Schema::TYPE_STRING . '(10) PRIMARY KEY NOT NULL',
	    ]);
	    //$this->createIndex('password_hash_idx', 'recovery_queries', 'password_hash', true);
    }

    public function down()
    {
        echo "m141020_162017_create_invites_table cannot be reverted.\n";

        return false;
    }
}
