<?php

use yii\db\Schema;
use yii\db\Migration;

class m150310_060643_change_type_auth_key_table extends Migration
{
    public function up()
    {
	    $this->alterColumn('auth_key', 'ip', Schema::TYPE_STRING . '(39) NOT NULL');
    }

    public function down()
    {
        echo "m150310_060643_change_type_auth_key_table cannot be reverted.\n";

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
