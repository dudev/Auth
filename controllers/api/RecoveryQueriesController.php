<?php

namespace app\controllers\api;

use app\models\RecoveryQueries;
use app\models\User;
use general\controllers\api\Controller;
use yii\db;

class RecoveryQueriesController extends Controller {
	public $layout = 'empty';
	public function actionNew($user_id, $service) {
		if(!User::findOne(['id' => $user_id])) {
			$errors = self::ERROR_NO_USER;
		} else {
			if($model = RecoveryQueries::newQuery($user_id, $service)) {
				return $this->sendSuccess([
					'hash' => $model->hash,
				]);
			} else {
				$errors = self::ERROR_DB;
			}
		}
		return $this->sendError(isset($errors) ?: self::ERROR_UNKNOWN);
	}
	public function actionСheckHash($hash) {
		if(RecoveryQueries::findOne(['hash' => $hash])) {
			return $this->sendSuccess([]);
		}
		return $this->sendError(self::ERROR_INCORRECT_HASH);
	}
	public function actionRestorePassword($hash, $password) {
		if($rq = RecoveryQueries::findOne(['hash' => $hash])) {
			if($user = User::findOne(['id' => $rq->user_id])) {
				$user->setAttribute('password', $password);
				if($user->hasErrors('password')) {
					$errors = self::ERROR_ILLEGAL_PASSWORD;
				} else {
					$user->setPassword($user->password);
					if($user->save()) {
						return $this->sendSuccess([]);
					} else {
						$errors = self::ERROR_DB;
					}
				}
			} else {
				$errors = self::ERROR_NO_USER;
			}
		} else {
			$errors = self::ERROR_INCORRECT_HASH;
		}
		return $this->sendError(isset($errors) ?: self::ERROR_UNKNOWN);
	}
}
